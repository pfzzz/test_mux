/*
 * MicroDexedControl.h
 *
 *  Created on: Dec 13, 2019
 *      Author: omar
 */

#ifndef MICRODEXEDCONTROL_H_
#define MICRODEXEDCONTROL_H_
#include "dexed.h"

class MicroDexedControl {
private:
	Dexed* dexed;
public:
	MicroDexedControl(Dexed* instance) {
		dexed = instance;
	}

	void update_coarse_tuning(int op, float value) {
		dexed->data[DEXED_OP_FREQ_COARSE + op * 21] = map(value, 0, 127, 0, 99);
	}

	void update_fine_tuning(int op, float value) {
		dexed->data[DEXED_OP_FREQ_FINE + op * 21] = map(value, 0, 127, 0, 99);
	}

    void update_output_level(int op, float value) {
        dexed->data[DEXED_OP_OUTPUT_LEV + op * 21] = map(value, 0, 127, 0, 99);
//        dexed->doRefreshVoice();
    }
};

#endif /* MICRODEXEDCONTROL_H_ */
