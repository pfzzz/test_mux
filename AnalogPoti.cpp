
#include "AnalogPoti.h"


AnalogPoti::AnalogPoti( uint8_t in_pin, uint16_t in_resolution, uint16_t in_map_to_max )
{
    pin = in_pin;
    resolution = in_resolution;
    map_to_max = in_map_to_max;

    poti_value = 0;
    poti_actionStartTime = 0;
}

bool AnalogPoti::read( int* value )
{
    bool potiValueChanged = false;

    int inputValue = analogRead( pin );

    bool potiIsInAction = ( millis() - poti_actionStartTime ) < ACTION_DURATION_MS;

    int delta = abs( inputValue - poti_value );

    if ( !potiIsInAction && delta < ACTION_START_THRESHOLD ) {
        // do nothing, it's just noise
    }
    else if ( !potiIsInAction && delta >= ACTION_START_THRESHOLD ) {

        // start action:
        poti_actionStartTime = millis();
        potiValueChanged = true;
    }
    else if ( potiIsInAction && delta > ACTION_LONGER_THRESHOLD ) {

        // roll over action:
        poti_actionStartTime = millis();
        potiValueChanged = true;
    }
    else if ( potiIsInAction && delta > 2 ) {

        potiValueChanged = true;
    }

    if ( potiValueChanged ) {

        poti_value = inputValue;

        float parameterValue_mapped = (float)poti_value * ((float)map_to_max + 1.0) / ((float)resolution + 1.0);
        parameter_value = floor( parameterValue_mapped );

#if PRINT_DEBUG_ANALOG_POTI
        Serial.print( "Poti[" );
        Serial.print( pin );
        Serial.print( "] poti_value = " );
        Serial.print( poti_value );
        Serial.print( ", parameter_value = " );
        Serial.println( parameter_value );
#endif
    }

    bool parameterValueChanged = ( parameter_value != last_parameter_value );

    if (parameterValueChanged) {

        *value = parameter_value;
        last_parameter_value = parameter_value;

#if PRINT_DEBUG_ANALOG_POTI_PARAMTER_VALUE_CHANGE
        Serial.print( "Poti[" );
        Serial.print( pin );
        Serial.print( "] parameter_value = " );
        Serial.println( parameter_value );
#endif
    }

    return parameterValueChanged;
}
