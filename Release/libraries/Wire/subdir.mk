################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
/home/omar/arduino/hardware/teensy/avr/libraries/Wire/Wire.cpp \
/home/omar/arduino/hardware/teensy/avr/libraries/Wire/WireIMXRT.cpp \
/home/omar/arduino/hardware/teensy/avr/libraries/Wire/WireKinetis.cpp 

LINK_OBJ += \
./libraries/Wire/Wire.cpp.o \
./libraries/Wire/WireIMXRT.cpp.o \
./libraries/Wire/WireKinetis.cpp.o 

CPP_DEPS += \
./libraries/Wire/Wire.cpp.d \
./libraries/Wire/WireIMXRT.cpp.d \
./libraries/Wire/WireKinetis.cpp.d 


# Each subdirectory must supply rules for building sources it contributes
libraries/Wire/Wire.cpp.o: /home/omar/arduino/hardware/teensy/avr/libraries/Wire/Wire.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-g++" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD -fno-exceptions -fpermissive -felide-constructors -std=gnu++14 -Wno-error=narrowing -fno-rtti -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL "-I/home/omar/workspace/arduino/test_mux/Release/pch"   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 -x c++ "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

libraries/Wire/WireIMXRT.cpp.o: /home/omar/arduino/hardware/teensy/avr/libraries/Wire/WireIMXRT.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-g++" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD -fno-exceptions -fpermissive -felide-constructors -std=gnu++14 -Wno-error=narrowing -fno-rtti -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL "-I/home/omar/workspace/arduino/test_mux/Release/pch"   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 -x c++ "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

libraries/Wire/WireKinetis.cpp.o: /home/omar/arduino/hardware/teensy/avr/libraries/Wire/WireKinetis.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-g++" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD -fno-exceptions -fpermissive -felide-constructors -std=gnu++14 -Wno-error=narrowing -fno-rtti -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL "-I/home/omar/workspace/arduino/test_mux/Release/pch"   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 -x c++ "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '


