################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
/home/omar/arduino/hardware/teensy/avr/libraries/Audio/analyze_fft1024.cpp \
/home/omar/arduino/hardware/teensy/avr/libraries/Audio/analyze_fft256.cpp \
/home/omar/arduino/hardware/teensy/avr/libraries/Audio/analyze_notefreq.cpp \
/home/omar/arduino/hardware/teensy/avr/libraries/Audio/analyze_peak.cpp \
/home/omar/arduino/hardware/teensy/avr/libraries/Audio/analyze_print.cpp \
/home/omar/arduino/hardware/teensy/avr/libraries/Audio/analyze_rms.cpp \
/home/omar/arduino/hardware/teensy/avr/libraries/Audio/analyze_tonedetect.cpp \
/home/omar/arduino/hardware/teensy/avr/libraries/Audio/control_ak4558.cpp \
/home/omar/arduino/hardware/teensy/avr/libraries/Audio/control_cs42448.cpp \
/home/omar/arduino/hardware/teensy/avr/libraries/Audio/control_cs4272.cpp \
/home/omar/arduino/hardware/teensy/avr/libraries/Audio/control_sgtl5000.cpp \
/home/omar/arduino/hardware/teensy/avr/libraries/Audio/control_tlv320aic3206.cpp \
/home/omar/arduino/hardware/teensy/avr/libraries/Audio/control_wm8731.cpp \
/home/omar/arduino/hardware/teensy/avr/libraries/Audio/effect_bitcrusher.cpp \
/home/omar/arduino/hardware/teensy/avr/libraries/Audio/effect_chorus.cpp \
/home/omar/arduino/hardware/teensy/avr/libraries/Audio/effect_combine.cpp \
/home/omar/arduino/hardware/teensy/avr/libraries/Audio/effect_delay.cpp \
/home/omar/arduino/hardware/teensy/avr/libraries/Audio/effect_delay_ext.cpp \
/home/omar/arduino/hardware/teensy/avr/libraries/Audio/effect_envelope.cpp \
/home/omar/arduino/hardware/teensy/avr/libraries/Audio/effect_fade.cpp \
/home/omar/arduino/hardware/teensy/avr/libraries/Audio/effect_flange.cpp \
/home/omar/arduino/hardware/teensy/avr/libraries/Audio/effect_freeverb.cpp \
/home/omar/arduino/hardware/teensy/avr/libraries/Audio/effect_granular.cpp \
/home/omar/arduino/hardware/teensy/avr/libraries/Audio/effect_midside.cpp \
/home/omar/arduino/hardware/teensy/avr/libraries/Audio/effect_multiply.cpp \
/home/omar/arduino/hardware/teensy/avr/libraries/Audio/effect_reverb.cpp \
/home/omar/arduino/hardware/teensy/avr/libraries/Audio/effect_waveshaper.cpp \
/home/omar/arduino/hardware/teensy/avr/libraries/Audio/filter_biquad.cpp \
/home/omar/arduino/hardware/teensy/avr/libraries/Audio/filter_fir.cpp \
/home/omar/arduino/hardware/teensy/avr/libraries/Audio/filter_variable.cpp \
/home/omar/arduino/hardware/teensy/avr/libraries/Audio/input_adc.cpp \
/home/omar/arduino/hardware/teensy/avr/libraries/Audio/input_adcs.cpp \
/home/omar/arduino/hardware/teensy/avr/libraries/Audio/input_i2s.cpp \
/home/omar/arduino/hardware/teensy/avr/libraries/Audio/input_i2s2.cpp \
/home/omar/arduino/hardware/teensy/avr/libraries/Audio/input_i2s_quad.cpp \
/home/omar/arduino/hardware/teensy/avr/libraries/Audio/input_pdm.cpp \
/home/omar/arduino/hardware/teensy/avr/libraries/Audio/input_tdm.cpp \
/home/omar/arduino/hardware/teensy/avr/libraries/Audio/input_tdm2.cpp \
/home/omar/arduino/hardware/teensy/avr/libraries/Audio/mixer.cpp \
/home/omar/arduino/hardware/teensy/avr/libraries/Audio/output_adat.cpp \
/home/omar/arduino/hardware/teensy/avr/libraries/Audio/output_dac.cpp \
/home/omar/arduino/hardware/teensy/avr/libraries/Audio/output_dacs.cpp \
/home/omar/arduino/hardware/teensy/avr/libraries/Audio/output_i2s.cpp \
/home/omar/arduino/hardware/teensy/avr/libraries/Audio/output_i2s2.cpp \
/home/omar/arduino/hardware/teensy/avr/libraries/Audio/output_i2s_quad.cpp \
/home/omar/arduino/hardware/teensy/avr/libraries/Audio/output_mqs.cpp \
/home/omar/arduino/hardware/teensy/avr/libraries/Audio/output_pt8211.cpp \
/home/omar/arduino/hardware/teensy/avr/libraries/Audio/output_pt8211_2.cpp \
/home/omar/arduino/hardware/teensy/avr/libraries/Audio/output_pwm.cpp \
/home/omar/arduino/hardware/teensy/avr/libraries/Audio/output_spdif.cpp \
/home/omar/arduino/hardware/teensy/avr/libraries/Audio/output_spdif2.cpp \
/home/omar/arduino/hardware/teensy/avr/libraries/Audio/output_spdif3.cpp \
/home/omar/arduino/hardware/teensy/avr/libraries/Audio/output_tdm.cpp \
/home/omar/arduino/hardware/teensy/avr/libraries/Audio/output_tdm2.cpp \
/home/omar/arduino/hardware/teensy/avr/libraries/Audio/play_memory.cpp \
/home/omar/arduino/hardware/teensy/avr/libraries/Audio/play_queue.cpp \
/home/omar/arduino/hardware/teensy/avr/libraries/Audio/play_sd_raw.cpp \
/home/omar/arduino/hardware/teensy/avr/libraries/Audio/play_sd_wav.cpp \
/home/omar/arduino/hardware/teensy/avr/libraries/Audio/play_serialflash_raw.cpp \
/home/omar/arduino/hardware/teensy/avr/libraries/Audio/record_queue.cpp \
/home/omar/arduino/hardware/teensy/avr/libraries/Audio/spi_interrupt.cpp \
/home/omar/arduino/hardware/teensy/avr/libraries/Audio/synth_dc.cpp \
/home/omar/arduino/hardware/teensy/avr/libraries/Audio/synth_karplusstrong.cpp \
/home/omar/arduino/hardware/teensy/avr/libraries/Audio/synth_pinknoise.cpp \
/home/omar/arduino/hardware/teensy/avr/libraries/Audio/synth_pwm.cpp \
/home/omar/arduino/hardware/teensy/avr/libraries/Audio/synth_simple_drum.cpp \
/home/omar/arduino/hardware/teensy/avr/libraries/Audio/synth_sine.cpp \
/home/omar/arduino/hardware/teensy/avr/libraries/Audio/synth_tonesweep.cpp \
/home/omar/arduino/hardware/teensy/avr/libraries/Audio/synth_waveform.cpp \
/home/omar/arduino/hardware/teensy/avr/libraries/Audio/synth_wavetable.cpp \
/home/omar/arduino/hardware/teensy/avr/libraries/Audio/synth_whitenoise.cpp 

S_UPPER_SRCS += \
/home/omar/arduino/hardware/teensy/avr/libraries/Audio/memcpy_audio.S 

C_SRCS += \
/home/omar/arduino/hardware/teensy/avr/libraries/Audio/data_spdif.c \
/home/omar/arduino/hardware/teensy/avr/libraries/Audio/data_ulaw.c \
/home/omar/arduino/hardware/teensy/avr/libraries/Audio/data_waveforms.c \
/home/omar/arduino/hardware/teensy/avr/libraries/Audio/data_windows.c 

C_DEPS += \
./libraries/Audio/data_spdif.c.d \
./libraries/Audio/data_ulaw.c.d \
./libraries/Audio/data_waveforms.c.d \
./libraries/Audio/data_windows.c.d 

LINK_OBJ += \
./libraries/Audio/analyze_fft1024.cpp.o \
./libraries/Audio/analyze_fft256.cpp.o \
./libraries/Audio/analyze_notefreq.cpp.o \
./libraries/Audio/analyze_peak.cpp.o \
./libraries/Audio/analyze_print.cpp.o \
./libraries/Audio/analyze_rms.cpp.o \
./libraries/Audio/analyze_tonedetect.cpp.o \
./libraries/Audio/control_ak4558.cpp.o \
./libraries/Audio/control_cs42448.cpp.o \
./libraries/Audio/control_cs4272.cpp.o \
./libraries/Audio/control_sgtl5000.cpp.o \
./libraries/Audio/control_tlv320aic3206.cpp.o \
./libraries/Audio/control_wm8731.cpp.o \
./libraries/Audio/data_spdif.c.o \
./libraries/Audio/data_ulaw.c.o \
./libraries/Audio/data_waveforms.c.o \
./libraries/Audio/data_windows.c.o \
./libraries/Audio/effect_bitcrusher.cpp.o \
./libraries/Audio/effect_chorus.cpp.o \
./libraries/Audio/effect_combine.cpp.o \
./libraries/Audio/effect_delay.cpp.o \
./libraries/Audio/effect_delay_ext.cpp.o \
./libraries/Audio/effect_envelope.cpp.o \
./libraries/Audio/effect_fade.cpp.o \
./libraries/Audio/effect_flange.cpp.o \
./libraries/Audio/effect_freeverb.cpp.o \
./libraries/Audio/effect_granular.cpp.o \
./libraries/Audio/effect_midside.cpp.o \
./libraries/Audio/effect_multiply.cpp.o \
./libraries/Audio/effect_reverb.cpp.o \
./libraries/Audio/effect_waveshaper.cpp.o \
./libraries/Audio/filter_biquad.cpp.o \
./libraries/Audio/filter_fir.cpp.o \
./libraries/Audio/filter_variable.cpp.o \
./libraries/Audio/input_adc.cpp.o \
./libraries/Audio/input_adcs.cpp.o \
./libraries/Audio/input_i2s.cpp.o \
./libraries/Audio/input_i2s2.cpp.o \
./libraries/Audio/input_i2s_quad.cpp.o \
./libraries/Audio/input_pdm.cpp.o \
./libraries/Audio/input_tdm.cpp.o \
./libraries/Audio/input_tdm2.cpp.o \
./libraries/Audio/memcpy_audio.S.o \
./libraries/Audio/mixer.cpp.o \
./libraries/Audio/output_adat.cpp.o \
./libraries/Audio/output_dac.cpp.o \
./libraries/Audio/output_dacs.cpp.o \
./libraries/Audio/output_i2s.cpp.o \
./libraries/Audio/output_i2s2.cpp.o \
./libraries/Audio/output_i2s_quad.cpp.o \
./libraries/Audio/output_mqs.cpp.o \
./libraries/Audio/output_pt8211.cpp.o \
./libraries/Audio/output_pt8211_2.cpp.o \
./libraries/Audio/output_pwm.cpp.o \
./libraries/Audio/output_spdif.cpp.o \
./libraries/Audio/output_spdif2.cpp.o \
./libraries/Audio/output_spdif3.cpp.o \
./libraries/Audio/output_tdm.cpp.o \
./libraries/Audio/output_tdm2.cpp.o \
./libraries/Audio/play_memory.cpp.o \
./libraries/Audio/play_queue.cpp.o \
./libraries/Audio/play_sd_raw.cpp.o \
./libraries/Audio/play_sd_wav.cpp.o \
./libraries/Audio/play_serialflash_raw.cpp.o \
./libraries/Audio/record_queue.cpp.o \
./libraries/Audio/spi_interrupt.cpp.o \
./libraries/Audio/synth_dc.cpp.o \
./libraries/Audio/synth_karplusstrong.cpp.o \
./libraries/Audio/synth_pinknoise.cpp.o \
./libraries/Audio/synth_pwm.cpp.o \
./libraries/Audio/synth_simple_drum.cpp.o \
./libraries/Audio/synth_sine.cpp.o \
./libraries/Audio/synth_tonesweep.cpp.o \
./libraries/Audio/synth_waveform.cpp.o \
./libraries/Audio/synth_wavetable.cpp.o \
./libraries/Audio/synth_whitenoise.cpp.o 

S_UPPER_DEPS += \
./libraries/Audio/memcpy_audio.S.d 

CPP_DEPS += \
./libraries/Audio/analyze_fft1024.cpp.d \
./libraries/Audio/analyze_fft256.cpp.d \
./libraries/Audio/analyze_notefreq.cpp.d \
./libraries/Audio/analyze_peak.cpp.d \
./libraries/Audio/analyze_print.cpp.d \
./libraries/Audio/analyze_rms.cpp.d \
./libraries/Audio/analyze_tonedetect.cpp.d \
./libraries/Audio/control_ak4558.cpp.d \
./libraries/Audio/control_cs42448.cpp.d \
./libraries/Audio/control_cs4272.cpp.d \
./libraries/Audio/control_sgtl5000.cpp.d \
./libraries/Audio/control_tlv320aic3206.cpp.d \
./libraries/Audio/control_wm8731.cpp.d \
./libraries/Audio/effect_bitcrusher.cpp.d \
./libraries/Audio/effect_chorus.cpp.d \
./libraries/Audio/effect_combine.cpp.d \
./libraries/Audio/effect_delay.cpp.d \
./libraries/Audio/effect_delay_ext.cpp.d \
./libraries/Audio/effect_envelope.cpp.d \
./libraries/Audio/effect_fade.cpp.d \
./libraries/Audio/effect_flange.cpp.d \
./libraries/Audio/effect_freeverb.cpp.d \
./libraries/Audio/effect_granular.cpp.d \
./libraries/Audio/effect_midside.cpp.d \
./libraries/Audio/effect_multiply.cpp.d \
./libraries/Audio/effect_reverb.cpp.d \
./libraries/Audio/effect_waveshaper.cpp.d \
./libraries/Audio/filter_biquad.cpp.d \
./libraries/Audio/filter_fir.cpp.d \
./libraries/Audio/filter_variable.cpp.d \
./libraries/Audio/input_adc.cpp.d \
./libraries/Audio/input_adcs.cpp.d \
./libraries/Audio/input_i2s.cpp.d \
./libraries/Audio/input_i2s2.cpp.d \
./libraries/Audio/input_i2s_quad.cpp.d \
./libraries/Audio/input_pdm.cpp.d \
./libraries/Audio/input_tdm.cpp.d \
./libraries/Audio/input_tdm2.cpp.d \
./libraries/Audio/mixer.cpp.d \
./libraries/Audio/output_adat.cpp.d \
./libraries/Audio/output_dac.cpp.d \
./libraries/Audio/output_dacs.cpp.d \
./libraries/Audio/output_i2s.cpp.d \
./libraries/Audio/output_i2s2.cpp.d \
./libraries/Audio/output_i2s_quad.cpp.d \
./libraries/Audio/output_mqs.cpp.d \
./libraries/Audio/output_pt8211.cpp.d \
./libraries/Audio/output_pt8211_2.cpp.d \
./libraries/Audio/output_pwm.cpp.d \
./libraries/Audio/output_spdif.cpp.d \
./libraries/Audio/output_spdif2.cpp.d \
./libraries/Audio/output_spdif3.cpp.d \
./libraries/Audio/output_tdm.cpp.d \
./libraries/Audio/output_tdm2.cpp.d \
./libraries/Audio/play_memory.cpp.d \
./libraries/Audio/play_queue.cpp.d \
./libraries/Audio/play_sd_raw.cpp.d \
./libraries/Audio/play_sd_wav.cpp.d \
./libraries/Audio/play_serialflash_raw.cpp.d \
./libraries/Audio/record_queue.cpp.d \
./libraries/Audio/spi_interrupt.cpp.d \
./libraries/Audio/synth_dc.cpp.d \
./libraries/Audio/synth_karplusstrong.cpp.d \
./libraries/Audio/synth_pinknoise.cpp.d \
./libraries/Audio/synth_pwm.cpp.d \
./libraries/Audio/synth_simple_drum.cpp.d \
./libraries/Audio/synth_sine.cpp.d \
./libraries/Audio/synth_tonesweep.cpp.d \
./libraries/Audio/synth_waveform.cpp.d \
./libraries/Audio/synth_wavetable.cpp.d \
./libraries/Audio/synth_whitenoise.cpp.d 


# Each subdirectory must supply rules for building sources it contributes
libraries/Audio/analyze_fft1024.cpp.o: /home/omar/arduino/hardware/teensy/avr/libraries/Audio/analyze_fft1024.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-g++" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD -fno-exceptions -fpermissive -felide-constructors -std=gnu++14 -Wno-error=narrowing -fno-rtti -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL "-I/home/omar/workspace/arduino/test_mux/Release/pch"   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 -x c++ "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

libraries/Audio/analyze_fft256.cpp.o: /home/omar/arduino/hardware/teensy/avr/libraries/Audio/analyze_fft256.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-g++" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD -fno-exceptions -fpermissive -felide-constructors -std=gnu++14 -Wno-error=narrowing -fno-rtti -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL "-I/home/omar/workspace/arduino/test_mux/Release/pch"   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 -x c++ "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

libraries/Audio/analyze_notefreq.cpp.o: /home/omar/arduino/hardware/teensy/avr/libraries/Audio/analyze_notefreq.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-g++" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD -fno-exceptions -fpermissive -felide-constructors -std=gnu++14 -Wno-error=narrowing -fno-rtti -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL "-I/home/omar/workspace/arduino/test_mux/Release/pch"   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 -x c++ "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

libraries/Audio/analyze_peak.cpp.o: /home/omar/arduino/hardware/teensy/avr/libraries/Audio/analyze_peak.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-g++" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD -fno-exceptions -fpermissive -felide-constructors -std=gnu++14 -Wno-error=narrowing -fno-rtti -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL "-I/home/omar/workspace/arduino/test_mux/Release/pch"   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 -x c++ "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

libraries/Audio/analyze_print.cpp.o: /home/omar/arduino/hardware/teensy/avr/libraries/Audio/analyze_print.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-g++" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD -fno-exceptions -fpermissive -felide-constructors -std=gnu++14 -Wno-error=narrowing -fno-rtti -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL "-I/home/omar/workspace/arduino/test_mux/Release/pch"   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 -x c++ "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

libraries/Audio/analyze_rms.cpp.o: /home/omar/arduino/hardware/teensy/avr/libraries/Audio/analyze_rms.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-g++" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD -fno-exceptions -fpermissive -felide-constructors -std=gnu++14 -Wno-error=narrowing -fno-rtti -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL "-I/home/omar/workspace/arduino/test_mux/Release/pch"   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 -x c++ "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

libraries/Audio/analyze_tonedetect.cpp.o: /home/omar/arduino/hardware/teensy/avr/libraries/Audio/analyze_tonedetect.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-g++" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD -fno-exceptions -fpermissive -felide-constructors -std=gnu++14 -Wno-error=narrowing -fno-rtti -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL "-I/home/omar/workspace/arduino/test_mux/Release/pch"   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 -x c++ "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

libraries/Audio/control_ak4558.cpp.o: /home/omar/arduino/hardware/teensy/avr/libraries/Audio/control_ak4558.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-g++" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD -fno-exceptions -fpermissive -felide-constructors -std=gnu++14 -Wno-error=narrowing -fno-rtti -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL "-I/home/omar/workspace/arduino/test_mux/Release/pch"   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 -x c++ "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

libraries/Audio/control_cs42448.cpp.o: /home/omar/arduino/hardware/teensy/avr/libraries/Audio/control_cs42448.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-g++" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD -fno-exceptions -fpermissive -felide-constructors -std=gnu++14 -Wno-error=narrowing -fno-rtti -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL "-I/home/omar/workspace/arduino/test_mux/Release/pch"   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 -x c++ "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

libraries/Audio/control_cs4272.cpp.o: /home/omar/arduino/hardware/teensy/avr/libraries/Audio/control_cs4272.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-g++" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD -fno-exceptions -fpermissive -felide-constructors -std=gnu++14 -Wno-error=narrowing -fno-rtti -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL "-I/home/omar/workspace/arduino/test_mux/Release/pch"   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 -x c++ "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

libraries/Audio/control_sgtl5000.cpp.o: /home/omar/arduino/hardware/teensy/avr/libraries/Audio/control_sgtl5000.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-g++" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD -fno-exceptions -fpermissive -felide-constructors -std=gnu++14 -Wno-error=narrowing -fno-rtti -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL "-I/home/omar/workspace/arduino/test_mux/Release/pch"   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 -x c++ "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

libraries/Audio/control_tlv320aic3206.cpp.o: /home/omar/arduino/hardware/teensy/avr/libraries/Audio/control_tlv320aic3206.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-g++" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD -fno-exceptions -fpermissive -felide-constructors -std=gnu++14 -Wno-error=narrowing -fno-rtti -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL "-I/home/omar/workspace/arduino/test_mux/Release/pch"   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 -x c++ "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

libraries/Audio/control_wm8731.cpp.o: /home/omar/arduino/hardware/teensy/avr/libraries/Audio/control_wm8731.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-g++" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD -fno-exceptions -fpermissive -felide-constructors -std=gnu++14 -Wno-error=narrowing -fno-rtti -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL "-I/home/omar/workspace/arduino/test_mux/Release/pch"   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 -x c++ "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

libraries/Audio/data_spdif.c.o: /home/omar/arduino/hardware/teensy/avr/libraries/Audio/data_spdif.c
	@echo 'Building file: $<'
	@echo 'Starting C compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-gcc" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD  -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

libraries/Audio/data_ulaw.c.o: /home/omar/arduino/hardware/teensy/avr/libraries/Audio/data_ulaw.c
	@echo 'Building file: $<'
	@echo 'Starting C compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-gcc" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD  -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

libraries/Audio/data_waveforms.c.o: /home/omar/arduino/hardware/teensy/avr/libraries/Audio/data_waveforms.c
	@echo 'Building file: $<'
	@echo 'Starting C compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-gcc" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD  -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

libraries/Audio/data_windows.c.o: /home/omar/arduino/hardware/teensy/avr/libraries/Audio/data_windows.c
	@echo 'Building file: $<'
	@echo 'Starting C compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-gcc" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD  -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

libraries/Audio/effect_bitcrusher.cpp.o: /home/omar/arduino/hardware/teensy/avr/libraries/Audio/effect_bitcrusher.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-g++" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD -fno-exceptions -fpermissive -felide-constructors -std=gnu++14 -Wno-error=narrowing -fno-rtti -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL "-I/home/omar/workspace/arduino/test_mux/Release/pch"   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 -x c++ "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

libraries/Audio/effect_chorus.cpp.o: /home/omar/arduino/hardware/teensy/avr/libraries/Audio/effect_chorus.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-g++" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD -fno-exceptions -fpermissive -felide-constructors -std=gnu++14 -Wno-error=narrowing -fno-rtti -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL "-I/home/omar/workspace/arduino/test_mux/Release/pch"   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 -x c++ "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

libraries/Audio/effect_combine.cpp.o: /home/omar/arduino/hardware/teensy/avr/libraries/Audio/effect_combine.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-g++" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD -fno-exceptions -fpermissive -felide-constructors -std=gnu++14 -Wno-error=narrowing -fno-rtti -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL "-I/home/omar/workspace/arduino/test_mux/Release/pch"   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 -x c++ "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

libraries/Audio/effect_delay.cpp.o: /home/omar/arduino/hardware/teensy/avr/libraries/Audio/effect_delay.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-g++" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD -fno-exceptions -fpermissive -felide-constructors -std=gnu++14 -Wno-error=narrowing -fno-rtti -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL "-I/home/omar/workspace/arduino/test_mux/Release/pch"   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 -x c++ "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

libraries/Audio/effect_delay_ext.cpp.o: /home/omar/arduino/hardware/teensy/avr/libraries/Audio/effect_delay_ext.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-g++" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD -fno-exceptions -fpermissive -felide-constructors -std=gnu++14 -Wno-error=narrowing -fno-rtti -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL "-I/home/omar/workspace/arduino/test_mux/Release/pch"   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 -x c++ "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

libraries/Audio/effect_envelope.cpp.o: /home/omar/arduino/hardware/teensy/avr/libraries/Audio/effect_envelope.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-g++" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD -fno-exceptions -fpermissive -felide-constructors -std=gnu++14 -Wno-error=narrowing -fno-rtti -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL "-I/home/omar/workspace/arduino/test_mux/Release/pch"   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 -x c++ "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

libraries/Audio/effect_fade.cpp.o: /home/omar/arduino/hardware/teensy/avr/libraries/Audio/effect_fade.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-g++" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD -fno-exceptions -fpermissive -felide-constructors -std=gnu++14 -Wno-error=narrowing -fno-rtti -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL "-I/home/omar/workspace/arduino/test_mux/Release/pch"   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 -x c++ "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

libraries/Audio/effect_flange.cpp.o: /home/omar/arduino/hardware/teensy/avr/libraries/Audio/effect_flange.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-g++" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD -fno-exceptions -fpermissive -felide-constructors -std=gnu++14 -Wno-error=narrowing -fno-rtti -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL "-I/home/omar/workspace/arduino/test_mux/Release/pch"   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 -x c++ "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

libraries/Audio/effect_freeverb.cpp.o: /home/omar/arduino/hardware/teensy/avr/libraries/Audio/effect_freeverb.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-g++" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD -fno-exceptions -fpermissive -felide-constructors -std=gnu++14 -Wno-error=narrowing -fno-rtti -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL "-I/home/omar/workspace/arduino/test_mux/Release/pch"   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 -x c++ "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

libraries/Audio/effect_granular.cpp.o: /home/omar/arduino/hardware/teensy/avr/libraries/Audio/effect_granular.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-g++" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD -fno-exceptions -fpermissive -felide-constructors -std=gnu++14 -Wno-error=narrowing -fno-rtti -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL "-I/home/omar/workspace/arduino/test_mux/Release/pch"   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 -x c++ "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

libraries/Audio/effect_midside.cpp.o: /home/omar/arduino/hardware/teensy/avr/libraries/Audio/effect_midside.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-g++" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD -fno-exceptions -fpermissive -felide-constructors -std=gnu++14 -Wno-error=narrowing -fno-rtti -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL "-I/home/omar/workspace/arduino/test_mux/Release/pch"   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 -x c++ "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

libraries/Audio/effect_multiply.cpp.o: /home/omar/arduino/hardware/teensy/avr/libraries/Audio/effect_multiply.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-g++" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD -fno-exceptions -fpermissive -felide-constructors -std=gnu++14 -Wno-error=narrowing -fno-rtti -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL "-I/home/omar/workspace/arduino/test_mux/Release/pch"   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 -x c++ "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

libraries/Audio/effect_reverb.cpp.o: /home/omar/arduino/hardware/teensy/avr/libraries/Audio/effect_reverb.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-g++" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD -fno-exceptions -fpermissive -felide-constructors -std=gnu++14 -Wno-error=narrowing -fno-rtti -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL "-I/home/omar/workspace/arduino/test_mux/Release/pch"   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 -x c++ "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

libraries/Audio/effect_waveshaper.cpp.o: /home/omar/arduino/hardware/teensy/avr/libraries/Audio/effect_waveshaper.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-g++" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD -fno-exceptions -fpermissive -felide-constructors -std=gnu++14 -Wno-error=narrowing -fno-rtti -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL "-I/home/omar/workspace/arduino/test_mux/Release/pch"   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 -x c++ "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

libraries/Audio/filter_biquad.cpp.o: /home/omar/arduino/hardware/teensy/avr/libraries/Audio/filter_biquad.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-g++" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD -fno-exceptions -fpermissive -felide-constructors -std=gnu++14 -Wno-error=narrowing -fno-rtti -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL "-I/home/omar/workspace/arduino/test_mux/Release/pch"   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 -x c++ "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

libraries/Audio/filter_fir.cpp.o: /home/omar/arduino/hardware/teensy/avr/libraries/Audio/filter_fir.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-g++" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD -fno-exceptions -fpermissive -felide-constructors -std=gnu++14 -Wno-error=narrowing -fno-rtti -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL "-I/home/omar/workspace/arduino/test_mux/Release/pch"   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 -x c++ "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

libraries/Audio/filter_variable.cpp.o: /home/omar/arduino/hardware/teensy/avr/libraries/Audio/filter_variable.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-g++" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD -fno-exceptions -fpermissive -felide-constructors -std=gnu++14 -Wno-error=narrowing -fno-rtti -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL "-I/home/omar/workspace/arduino/test_mux/Release/pch"   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 -x c++ "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

libraries/Audio/input_adc.cpp.o: /home/omar/arduino/hardware/teensy/avr/libraries/Audio/input_adc.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-g++" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD -fno-exceptions -fpermissive -felide-constructors -std=gnu++14 -Wno-error=narrowing -fno-rtti -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL "-I/home/omar/workspace/arduino/test_mux/Release/pch"   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 -x c++ "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

libraries/Audio/input_adcs.cpp.o: /home/omar/arduino/hardware/teensy/avr/libraries/Audio/input_adcs.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-g++" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD -fno-exceptions -fpermissive -felide-constructors -std=gnu++14 -Wno-error=narrowing -fno-rtti -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL "-I/home/omar/workspace/arduino/test_mux/Release/pch"   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 -x c++ "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

libraries/Audio/input_i2s.cpp.o: /home/omar/arduino/hardware/teensy/avr/libraries/Audio/input_i2s.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-g++" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD -fno-exceptions -fpermissive -felide-constructors -std=gnu++14 -Wno-error=narrowing -fno-rtti -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL "-I/home/omar/workspace/arduino/test_mux/Release/pch"   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 -x c++ "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

libraries/Audio/input_i2s2.cpp.o: /home/omar/arduino/hardware/teensy/avr/libraries/Audio/input_i2s2.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-g++" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD -fno-exceptions -fpermissive -felide-constructors -std=gnu++14 -Wno-error=narrowing -fno-rtti -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL "-I/home/omar/workspace/arduino/test_mux/Release/pch"   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 -x c++ "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

libraries/Audio/input_i2s_quad.cpp.o: /home/omar/arduino/hardware/teensy/avr/libraries/Audio/input_i2s_quad.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-g++" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD -fno-exceptions -fpermissive -felide-constructors -std=gnu++14 -Wno-error=narrowing -fno-rtti -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL "-I/home/omar/workspace/arduino/test_mux/Release/pch"   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 -x c++ "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

libraries/Audio/input_pdm.cpp.o: /home/omar/arduino/hardware/teensy/avr/libraries/Audio/input_pdm.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-g++" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD -fno-exceptions -fpermissive -felide-constructors -std=gnu++14 -Wno-error=narrowing -fno-rtti -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL "-I/home/omar/workspace/arduino/test_mux/Release/pch"   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 -x c++ "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

libraries/Audio/input_tdm.cpp.o: /home/omar/arduino/hardware/teensy/avr/libraries/Audio/input_tdm.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-g++" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD -fno-exceptions -fpermissive -felide-constructors -std=gnu++14 -Wno-error=narrowing -fno-rtti -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL "-I/home/omar/workspace/arduino/test_mux/Release/pch"   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 -x c++ "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

libraries/Audio/input_tdm2.cpp.o: /home/omar/arduino/hardware/teensy/avr/libraries/Audio/input_tdm2.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-g++" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD -fno-exceptions -fpermissive -felide-constructors -std=gnu++14 -Wno-error=narrowing -fno-rtti -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL "-I/home/omar/workspace/arduino/test_mux/Release/pch"   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 -x c++ "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

libraries/Audio/memcpy_audio.S.o: /home/omar/arduino/hardware/teensy/avr/libraries/Audio/memcpy_audio.S
	@echo 'Building file: $<'
	@echo 'Starting S compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-gcc" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD -x assembler-with-cpp -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)"  "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

libraries/Audio/mixer.cpp.o: /home/omar/arduino/hardware/teensy/avr/libraries/Audio/mixer.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-g++" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD -fno-exceptions -fpermissive -felide-constructors -std=gnu++14 -Wno-error=narrowing -fno-rtti -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL "-I/home/omar/workspace/arduino/test_mux/Release/pch"   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 -x c++ "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

libraries/Audio/output_adat.cpp.o: /home/omar/arduino/hardware/teensy/avr/libraries/Audio/output_adat.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-g++" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD -fno-exceptions -fpermissive -felide-constructors -std=gnu++14 -Wno-error=narrowing -fno-rtti -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL "-I/home/omar/workspace/arduino/test_mux/Release/pch"   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 -x c++ "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

libraries/Audio/output_dac.cpp.o: /home/omar/arduino/hardware/teensy/avr/libraries/Audio/output_dac.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-g++" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD -fno-exceptions -fpermissive -felide-constructors -std=gnu++14 -Wno-error=narrowing -fno-rtti -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL "-I/home/omar/workspace/arduino/test_mux/Release/pch"   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 -x c++ "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

libraries/Audio/output_dacs.cpp.o: /home/omar/arduino/hardware/teensy/avr/libraries/Audio/output_dacs.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-g++" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD -fno-exceptions -fpermissive -felide-constructors -std=gnu++14 -Wno-error=narrowing -fno-rtti -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL "-I/home/omar/workspace/arduino/test_mux/Release/pch"   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 -x c++ "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

libraries/Audio/output_i2s.cpp.o: /home/omar/arduino/hardware/teensy/avr/libraries/Audio/output_i2s.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-g++" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD -fno-exceptions -fpermissive -felide-constructors -std=gnu++14 -Wno-error=narrowing -fno-rtti -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL "-I/home/omar/workspace/arduino/test_mux/Release/pch"   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 -x c++ "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

libraries/Audio/output_i2s2.cpp.o: /home/omar/arduino/hardware/teensy/avr/libraries/Audio/output_i2s2.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-g++" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD -fno-exceptions -fpermissive -felide-constructors -std=gnu++14 -Wno-error=narrowing -fno-rtti -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL "-I/home/omar/workspace/arduino/test_mux/Release/pch"   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 -x c++ "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

libraries/Audio/output_i2s_quad.cpp.o: /home/omar/arduino/hardware/teensy/avr/libraries/Audio/output_i2s_quad.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-g++" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD -fno-exceptions -fpermissive -felide-constructors -std=gnu++14 -Wno-error=narrowing -fno-rtti -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL "-I/home/omar/workspace/arduino/test_mux/Release/pch"   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 -x c++ "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

libraries/Audio/output_mqs.cpp.o: /home/omar/arduino/hardware/teensy/avr/libraries/Audio/output_mqs.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-g++" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD -fno-exceptions -fpermissive -felide-constructors -std=gnu++14 -Wno-error=narrowing -fno-rtti -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL "-I/home/omar/workspace/arduino/test_mux/Release/pch"   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 -x c++ "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

libraries/Audio/output_pt8211.cpp.o: /home/omar/arduino/hardware/teensy/avr/libraries/Audio/output_pt8211.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-g++" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD -fno-exceptions -fpermissive -felide-constructors -std=gnu++14 -Wno-error=narrowing -fno-rtti -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL "-I/home/omar/workspace/arduino/test_mux/Release/pch"   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 -x c++ "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

libraries/Audio/output_pt8211_2.cpp.o: /home/omar/arduino/hardware/teensy/avr/libraries/Audio/output_pt8211_2.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-g++" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD -fno-exceptions -fpermissive -felide-constructors -std=gnu++14 -Wno-error=narrowing -fno-rtti -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL "-I/home/omar/workspace/arduino/test_mux/Release/pch"   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 -x c++ "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

libraries/Audio/output_pwm.cpp.o: /home/omar/arduino/hardware/teensy/avr/libraries/Audio/output_pwm.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-g++" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD -fno-exceptions -fpermissive -felide-constructors -std=gnu++14 -Wno-error=narrowing -fno-rtti -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL "-I/home/omar/workspace/arduino/test_mux/Release/pch"   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 -x c++ "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

libraries/Audio/output_spdif.cpp.o: /home/omar/arduino/hardware/teensy/avr/libraries/Audio/output_spdif.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-g++" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD -fno-exceptions -fpermissive -felide-constructors -std=gnu++14 -Wno-error=narrowing -fno-rtti -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL "-I/home/omar/workspace/arduino/test_mux/Release/pch"   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 -x c++ "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

libraries/Audio/output_spdif2.cpp.o: /home/omar/arduino/hardware/teensy/avr/libraries/Audio/output_spdif2.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-g++" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD -fno-exceptions -fpermissive -felide-constructors -std=gnu++14 -Wno-error=narrowing -fno-rtti -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL "-I/home/omar/workspace/arduino/test_mux/Release/pch"   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 -x c++ "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

libraries/Audio/output_spdif3.cpp.o: /home/omar/arduino/hardware/teensy/avr/libraries/Audio/output_spdif3.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-g++" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD -fno-exceptions -fpermissive -felide-constructors -std=gnu++14 -Wno-error=narrowing -fno-rtti -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL "-I/home/omar/workspace/arduino/test_mux/Release/pch"   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 -x c++ "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

libraries/Audio/output_tdm.cpp.o: /home/omar/arduino/hardware/teensy/avr/libraries/Audio/output_tdm.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-g++" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD -fno-exceptions -fpermissive -felide-constructors -std=gnu++14 -Wno-error=narrowing -fno-rtti -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL "-I/home/omar/workspace/arduino/test_mux/Release/pch"   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 -x c++ "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

libraries/Audio/output_tdm2.cpp.o: /home/omar/arduino/hardware/teensy/avr/libraries/Audio/output_tdm2.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-g++" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD -fno-exceptions -fpermissive -felide-constructors -std=gnu++14 -Wno-error=narrowing -fno-rtti -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL "-I/home/omar/workspace/arduino/test_mux/Release/pch"   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 -x c++ "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

libraries/Audio/play_memory.cpp.o: /home/omar/arduino/hardware/teensy/avr/libraries/Audio/play_memory.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-g++" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD -fno-exceptions -fpermissive -felide-constructors -std=gnu++14 -Wno-error=narrowing -fno-rtti -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL "-I/home/omar/workspace/arduino/test_mux/Release/pch"   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 -x c++ "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

libraries/Audio/play_queue.cpp.o: /home/omar/arduino/hardware/teensy/avr/libraries/Audio/play_queue.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-g++" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD -fno-exceptions -fpermissive -felide-constructors -std=gnu++14 -Wno-error=narrowing -fno-rtti -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL "-I/home/omar/workspace/arduino/test_mux/Release/pch"   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 -x c++ "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

libraries/Audio/play_sd_raw.cpp.o: /home/omar/arduino/hardware/teensy/avr/libraries/Audio/play_sd_raw.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-g++" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD -fno-exceptions -fpermissive -felide-constructors -std=gnu++14 -Wno-error=narrowing -fno-rtti -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL "-I/home/omar/workspace/arduino/test_mux/Release/pch"   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 -x c++ "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

libraries/Audio/play_sd_wav.cpp.o: /home/omar/arduino/hardware/teensy/avr/libraries/Audio/play_sd_wav.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-g++" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD -fno-exceptions -fpermissive -felide-constructors -std=gnu++14 -Wno-error=narrowing -fno-rtti -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL "-I/home/omar/workspace/arduino/test_mux/Release/pch"   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 -x c++ "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

libraries/Audio/play_serialflash_raw.cpp.o: /home/omar/arduino/hardware/teensy/avr/libraries/Audio/play_serialflash_raw.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-g++" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD -fno-exceptions -fpermissive -felide-constructors -std=gnu++14 -Wno-error=narrowing -fno-rtti -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL "-I/home/omar/workspace/arduino/test_mux/Release/pch"   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 -x c++ "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

libraries/Audio/record_queue.cpp.o: /home/omar/arduino/hardware/teensy/avr/libraries/Audio/record_queue.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-g++" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD -fno-exceptions -fpermissive -felide-constructors -std=gnu++14 -Wno-error=narrowing -fno-rtti -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL "-I/home/omar/workspace/arduino/test_mux/Release/pch"   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 -x c++ "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

libraries/Audio/spi_interrupt.cpp.o: /home/omar/arduino/hardware/teensy/avr/libraries/Audio/spi_interrupt.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-g++" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD -fno-exceptions -fpermissive -felide-constructors -std=gnu++14 -Wno-error=narrowing -fno-rtti -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL "-I/home/omar/workspace/arduino/test_mux/Release/pch"   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 -x c++ "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

libraries/Audio/synth_dc.cpp.o: /home/omar/arduino/hardware/teensy/avr/libraries/Audio/synth_dc.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-g++" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD -fno-exceptions -fpermissive -felide-constructors -std=gnu++14 -Wno-error=narrowing -fno-rtti -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL "-I/home/omar/workspace/arduino/test_mux/Release/pch"   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 -x c++ "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

libraries/Audio/synth_karplusstrong.cpp.o: /home/omar/arduino/hardware/teensy/avr/libraries/Audio/synth_karplusstrong.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-g++" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD -fno-exceptions -fpermissive -felide-constructors -std=gnu++14 -Wno-error=narrowing -fno-rtti -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL "-I/home/omar/workspace/arduino/test_mux/Release/pch"   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 -x c++ "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

libraries/Audio/synth_pinknoise.cpp.o: /home/omar/arduino/hardware/teensy/avr/libraries/Audio/synth_pinknoise.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-g++" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD -fno-exceptions -fpermissive -felide-constructors -std=gnu++14 -Wno-error=narrowing -fno-rtti -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL "-I/home/omar/workspace/arduino/test_mux/Release/pch"   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 -x c++ "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

libraries/Audio/synth_pwm.cpp.o: /home/omar/arduino/hardware/teensy/avr/libraries/Audio/synth_pwm.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-g++" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD -fno-exceptions -fpermissive -felide-constructors -std=gnu++14 -Wno-error=narrowing -fno-rtti -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL "-I/home/omar/workspace/arduino/test_mux/Release/pch"   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 -x c++ "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

libraries/Audio/synth_simple_drum.cpp.o: /home/omar/arduino/hardware/teensy/avr/libraries/Audio/synth_simple_drum.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-g++" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD -fno-exceptions -fpermissive -felide-constructors -std=gnu++14 -Wno-error=narrowing -fno-rtti -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL "-I/home/omar/workspace/arduino/test_mux/Release/pch"   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 -x c++ "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

libraries/Audio/synth_sine.cpp.o: /home/omar/arduino/hardware/teensy/avr/libraries/Audio/synth_sine.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-g++" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD -fno-exceptions -fpermissive -felide-constructors -std=gnu++14 -Wno-error=narrowing -fno-rtti -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL "-I/home/omar/workspace/arduino/test_mux/Release/pch"   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 -x c++ "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

libraries/Audio/synth_tonesweep.cpp.o: /home/omar/arduino/hardware/teensy/avr/libraries/Audio/synth_tonesweep.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-g++" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD -fno-exceptions -fpermissive -felide-constructors -std=gnu++14 -Wno-error=narrowing -fno-rtti -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL "-I/home/omar/workspace/arduino/test_mux/Release/pch"   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 -x c++ "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

libraries/Audio/synth_waveform.cpp.o: /home/omar/arduino/hardware/teensy/avr/libraries/Audio/synth_waveform.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-g++" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD -fno-exceptions -fpermissive -felide-constructors -std=gnu++14 -Wno-error=narrowing -fno-rtti -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL "-I/home/omar/workspace/arduino/test_mux/Release/pch"   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 -x c++ "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

libraries/Audio/synth_wavetable.cpp.o: /home/omar/arduino/hardware/teensy/avr/libraries/Audio/synth_wavetable.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-g++" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD -fno-exceptions -fpermissive -felide-constructors -std=gnu++14 -Wno-error=narrowing -fno-rtti -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL "-I/home/omar/workspace/arduino/test_mux/Release/pch"   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 -x c++ "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

libraries/Audio/synth_whitenoise.cpp.o: /home/omar/arduino/hardware/teensy/avr/libraries/Audio/synth_whitenoise.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-g++" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD -fno-exceptions -fpermissive -felide-constructors -std=gnu++14 -Wno-error=narrowing -fno-rtti -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL "-I/home/omar/workspace/arduino/test_mux/Release/pch"   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 -x c++ "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '


