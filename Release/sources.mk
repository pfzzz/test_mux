################################################################################
# Automatically-generated file. Do not edit!
################################################################################

INO_SRCS := 
ASM_SRCS := 
O_UPPER_SRCS := 
CPP_SRCS := 
S_UPPER_SRCS := 
O_SRCS := 
ELF_SRCS := 
C_UPPER_SRCS := 
CXX_SRCS := 
C++_SRCS := 
PDE_SRCS := 
CC_SRCS := 
C_SRCS := 
C_UPPER_DEPS := 
PDE_DEPS := 
C_DEPS := 
AR := 
CC_DEPS := 
AR_OBJ := 
C++_DEPS := 
LINK_OBJ := 
CXX_DEPS := 
ASM_DEPS := 
HEX := 
INO_DEPS := 
SIZEDUMMY := 
S_UPPER_DEPS := 
ELF := 
CPP_DEPS := 

# Every subdirectory with source files must be described here
SUBDIRS := \
core/core \
. \
libraries/Audio \
libraries/Audio/utility \
libraries/MUX74HC4067 \
libraries/SD \
libraries/SD/utility \
libraries/SPI \
libraries/SerialFlash \
libraries/Wire \
libraries/Wire/utility \
libraries/movingAvg/src \
third-party/Bounce2/examples/bounce \
third-party/Bounce2/examples/bounce2buttons \
third-party/Bounce2/examples/bounce_multiple \
third-party/Bounce2/examples/change \
third-party/Bounce2/examples/duration \
third-party/Bounce2/examples/retrigger \
third-party/Bounce2/src \
third-party/LiquidCrystal_I2C \
third-party/LiquidCrystal_I2C/examples/CustomChars \
third-party/LiquidCrystal_I2C/examples/HelloWorld \
third-party/LiquidCrystal_I2C/examples/SerialDisplay \
third-party/ResponsiveAnalogRead/examples/ResponsiveAnalogRead \
third-party/ResponsiveAnalogRead/src \

