################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
/home/omar/arduino/hardware/teensy/avr/cores/teensy3/AudioStream.cpp \
/home/omar/arduino/hardware/teensy/avr/cores/teensy3/DMAChannel.cpp \
/home/omar/arduino/hardware/teensy/avr/cores/teensy3/EventResponder.cpp \
/home/omar/arduino/hardware/teensy/avr/cores/teensy3/HardwareSerial1.cpp \
/home/omar/arduino/hardware/teensy/avr/cores/teensy3/HardwareSerial2.cpp \
/home/omar/arduino/hardware/teensy/avr/cores/teensy3/HardwareSerial3.cpp \
/home/omar/arduino/hardware/teensy/avr/cores/teensy3/HardwareSerial4.cpp \
/home/omar/arduino/hardware/teensy/avr/cores/teensy3/HardwareSerial5.cpp \
/home/omar/arduino/hardware/teensy/avr/cores/teensy3/HardwareSerial6.cpp \
/home/omar/arduino/hardware/teensy/avr/cores/teensy3/IPAddress.cpp \
/home/omar/arduino/hardware/teensy/avr/cores/teensy3/IntervalTimer.cpp \
/home/omar/arduino/hardware/teensy/avr/cores/teensy3/Print.cpp \
/home/omar/arduino/hardware/teensy/avr/cores/teensy3/Stream.cpp \
/home/omar/arduino/hardware/teensy/avr/cores/teensy3/Tone.cpp \
/home/omar/arduino/hardware/teensy/avr/cores/teensy3/WMath.cpp \
/home/omar/arduino/hardware/teensy/avr/cores/teensy3/WString.cpp \
/home/omar/arduino/hardware/teensy/avr/cores/teensy3/avr_emulation.cpp \
/home/omar/arduino/hardware/teensy/avr/cores/teensy3/main.cpp \
/home/omar/arduino/hardware/teensy/avr/cores/teensy3/new.cpp \
/home/omar/arduino/hardware/teensy/avr/cores/teensy3/usb_audio.cpp \
/home/omar/arduino/hardware/teensy/avr/cores/teensy3/usb_flightsim.cpp \
/home/omar/arduino/hardware/teensy/avr/cores/teensy3/usb_inst.cpp \
/home/omar/arduino/hardware/teensy/avr/cores/teensy3/yield.cpp 

S_UPPER_SRCS += \
/home/omar/arduino/hardware/teensy/avr/cores/teensy3/memcpy-armv7m.S \
/home/omar/arduino/hardware/teensy/avr/cores/teensy3/memset.S 

C_SRCS += \
/home/omar/arduino/hardware/teensy/avr/cores/teensy3/analog.c \
/home/omar/arduino/hardware/teensy/avr/cores/teensy3/eeprom.c \
/home/omar/arduino/hardware/teensy/avr/cores/teensy3/keylayouts.c \
/home/omar/arduino/hardware/teensy/avr/cores/teensy3/math_helper.c \
/home/omar/arduino/hardware/teensy/avr/cores/teensy3/mk20dx128.c \
/home/omar/arduino/hardware/teensy/avr/cores/teensy3/nonstd.c \
/home/omar/arduino/hardware/teensy/avr/cores/teensy3/pins_teensy.c \
/home/omar/arduino/hardware/teensy/avr/cores/teensy3/ser_print.c \
/home/omar/arduino/hardware/teensy/avr/cores/teensy3/serial1.c \
/home/omar/arduino/hardware/teensy/avr/cores/teensy3/serial2.c \
/home/omar/arduino/hardware/teensy/avr/cores/teensy3/serial3.c \
/home/omar/arduino/hardware/teensy/avr/cores/teensy3/serial4.c \
/home/omar/arduino/hardware/teensy/avr/cores/teensy3/serial5.c \
/home/omar/arduino/hardware/teensy/avr/cores/teensy3/serial6.c \
/home/omar/arduino/hardware/teensy/avr/cores/teensy3/serial6_lpuart.c \
/home/omar/arduino/hardware/teensy/avr/cores/teensy3/touch.c \
/home/omar/arduino/hardware/teensy/avr/cores/teensy3/usb_desc.c \
/home/omar/arduino/hardware/teensy/avr/cores/teensy3/usb_dev.c \
/home/omar/arduino/hardware/teensy/avr/cores/teensy3/usb_joystick.c \
/home/omar/arduino/hardware/teensy/avr/cores/teensy3/usb_keyboard.c \
/home/omar/arduino/hardware/teensy/avr/cores/teensy3/usb_mem.c \
/home/omar/arduino/hardware/teensy/avr/cores/teensy3/usb_midi.c \
/home/omar/arduino/hardware/teensy/avr/cores/teensy3/usb_mouse.c \
/home/omar/arduino/hardware/teensy/avr/cores/teensy3/usb_mtp.c \
/home/omar/arduino/hardware/teensy/avr/cores/teensy3/usb_rawhid.c \
/home/omar/arduino/hardware/teensy/avr/cores/teensy3/usb_seremu.c \
/home/omar/arduino/hardware/teensy/avr/cores/teensy3/usb_serial.c \
/home/omar/arduino/hardware/teensy/avr/cores/teensy3/usb_touch.c 

C_DEPS += \
./core/core/analog.c.d \
./core/core/eeprom.c.d \
./core/core/keylayouts.c.d \
./core/core/math_helper.c.d \
./core/core/mk20dx128.c.d \
./core/core/nonstd.c.d \
./core/core/pins_teensy.c.d \
./core/core/ser_print.c.d \
./core/core/serial1.c.d \
./core/core/serial2.c.d \
./core/core/serial3.c.d \
./core/core/serial4.c.d \
./core/core/serial5.c.d \
./core/core/serial6.c.d \
./core/core/serial6_lpuart.c.d \
./core/core/touch.c.d \
./core/core/usb_desc.c.d \
./core/core/usb_dev.c.d \
./core/core/usb_joystick.c.d \
./core/core/usb_keyboard.c.d \
./core/core/usb_mem.c.d \
./core/core/usb_midi.c.d \
./core/core/usb_mouse.c.d \
./core/core/usb_mtp.c.d \
./core/core/usb_rawhid.c.d \
./core/core/usb_seremu.c.d \
./core/core/usb_serial.c.d \
./core/core/usb_touch.c.d 

AR_OBJ += \
./core/core/AudioStream.cpp.o \
./core/core/DMAChannel.cpp.o \
./core/core/EventResponder.cpp.o \
./core/core/HardwareSerial1.cpp.o \
./core/core/HardwareSerial2.cpp.o \
./core/core/HardwareSerial3.cpp.o \
./core/core/HardwareSerial4.cpp.o \
./core/core/HardwareSerial5.cpp.o \
./core/core/HardwareSerial6.cpp.o \
./core/core/IPAddress.cpp.o \
./core/core/IntervalTimer.cpp.o \
./core/core/Print.cpp.o \
./core/core/Stream.cpp.o \
./core/core/Tone.cpp.o \
./core/core/WMath.cpp.o \
./core/core/WString.cpp.o \
./core/core/analog.c.o \
./core/core/avr_emulation.cpp.o \
./core/core/eeprom.c.o \
./core/core/keylayouts.c.o \
./core/core/main.cpp.o \
./core/core/math_helper.c.o \
./core/core/memcpy-armv7m.S.o \
./core/core/memset.S.o \
./core/core/mk20dx128.c.o \
./core/core/new.cpp.o \
./core/core/nonstd.c.o \
./core/core/pins_teensy.c.o \
./core/core/ser_print.c.o \
./core/core/serial1.c.o \
./core/core/serial2.c.o \
./core/core/serial3.c.o \
./core/core/serial4.c.o \
./core/core/serial5.c.o \
./core/core/serial6.c.o \
./core/core/serial6_lpuart.c.o \
./core/core/touch.c.o \
./core/core/usb_audio.cpp.o \
./core/core/usb_desc.c.o \
./core/core/usb_dev.c.o \
./core/core/usb_flightsim.cpp.o \
./core/core/usb_inst.cpp.o \
./core/core/usb_joystick.c.o \
./core/core/usb_keyboard.c.o \
./core/core/usb_mem.c.o \
./core/core/usb_midi.c.o \
./core/core/usb_mouse.c.o \
./core/core/usb_mtp.c.o \
./core/core/usb_rawhid.c.o \
./core/core/usb_seremu.c.o \
./core/core/usb_serial.c.o \
./core/core/usb_touch.c.o \
./core/core/yield.cpp.o 

S_UPPER_DEPS += \
./core/core/memcpy-armv7m.S.d \
./core/core/memset.S.d 

CPP_DEPS += \
./core/core/AudioStream.cpp.d \
./core/core/DMAChannel.cpp.d \
./core/core/EventResponder.cpp.d \
./core/core/HardwareSerial1.cpp.d \
./core/core/HardwareSerial2.cpp.d \
./core/core/HardwareSerial3.cpp.d \
./core/core/HardwareSerial4.cpp.d \
./core/core/HardwareSerial5.cpp.d \
./core/core/HardwareSerial6.cpp.d \
./core/core/IPAddress.cpp.d \
./core/core/IntervalTimer.cpp.d \
./core/core/Print.cpp.d \
./core/core/Stream.cpp.d \
./core/core/Tone.cpp.d \
./core/core/WMath.cpp.d \
./core/core/WString.cpp.d \
./core/core/avr_emulation.cpp.d \
./core/core/main.cpp.d \
./core/core/new.cpp.d \
./core/core/usb_audio.cpp.d \
./core/core/usb_flightsim.cpp.d \
./core/core/usb_inst.cpp.d \
./core/core/yield.cpp.d 


# Each subdirectory must supply rules for building sources it contributes
core/core/AudioStream.cpp.o: /home/omar/arduino/hardware/teensy/avr/cores/teensy3/AudioStream.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-g++" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD -fno-exceptions -fpermissive -felide-constructors -std=gnu++14 -Wno-error=narrowing -fno-rtti -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL "-I/home/omar/workspace/arduino/test_mux/Release/pch"   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 -x c++ "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

core/core/DMAChannel.cpp.o: /home/omar/arduino/hardware/teensy/avr/cores/teensy3/DMAChannel.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-g++" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD -fno-exceptions -fpermissive -felide-constructors -std=gnu++14 -Wno-error=narrowing -fno-rtti -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL "-I/home/omar/workspace/arduino/test_mux/Release/pch"   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 -x c++ "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

core/core/EventResponder.cpp.o: /home/omar/arduino/hardware/teensy/avr/cores/teensy3/EventResponder.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-g++" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD -fno-exceptions -fpermissive -felide-constructors -std=gnu++14 -Wno-error=narrowing -fno-rtti -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL "-I/home/omar/workspace/arduino/test_mux/Release/pch"   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 -x c++ "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

core/core/HardwareSerial1.cpp.o: /home/omar/arduino/hardware/teensy/avr/cores/teensy3/HardwareSerial1.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-g++" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD -fno-exceptions -fpermissive -felide-constructors -std=gnu++14 -Wno-error=narrowing -fno-rtti -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL "-I/home/omar/workspace/arduino/test_mux/Release/pch"   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 -x c++ "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

core/core/HardwareSerial2.cpp.o: /home/omar/arduino/hardware/teensy/avr/cores/teensy3/HardwareSerial2.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-g++" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD -fno-exceptions -fpermissive -felide-constructors -std=gnu++14 -Wno-error=narrowing -fno-rtti -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL "-I/home/omar/workspace/arduino/test_mux/Release/pch"   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 -x c++ "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

core/core/HardwareSerial3.cpp.o: /home/omar/arduino/hardware/teensy/avr/cores/teensy3/HardwareSerial3.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-g++" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD -fno-exceptions -fpermissive -felide-constructors -std=gnu++14 -Wno-error=narrowing -fno-rtti -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL "-I/home/omar/workspace/arduino/test_mux/Release/pch"   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 -x c++ "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

core/core/HardwareSerial4.cpp.o: /home/omar/arduino/hardware/teensy/avr/cores/teensy3/HardwareSerial4.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-g++" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD -fno-exceptions -fpermissive -felide-constructors -std=gnu++14 -Wno-error=narrowing -fno-rtti -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL "-I/home/omar/workspace/arduino/test_mux/Release/pch"   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 -x c++ "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

core/core/HardwareSerial5.cpp.o: /home/omar/arduino/hardware/teensy/avr/cores/teensy3/HardwareSerial5.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-g++" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD -fno-exceptions -fpermissive -felide-constructors -std=gnu++14 -Wno-error=narrowing -fno-rtti -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL "-I/home/omar/workspace/arduino/test_mux/Release/pch"   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 -x c++ "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

core/core/HardwareSerial6.cpp.o: /home/omar/arduino/hardware/teensy/avr/cores/teensy3/HardwareSerial6.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-g++" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD -fno-exceptions -fpermissive -felide-constructors -std=gnu++14 -Wno-error=narrowing -fno-rtti -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL "-I/home/omar/workspace/arduino/test_mux/Release/pch"   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 -x c++ "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

core/core/IPAddress.cpp.o: /home/omar/arduino/hardware/teensy/avr/cores/teensy3/IPAddress.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-g++" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD -fno-exceptions -fpermissive -felide-constructors -std=gnu++14 -Wno-error=narrowing -fno-rtti -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL "-I/home/omar/workspace/arduino/test_mux/Release/pch"   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 -x c++ "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

core/core/IntervalTimer.cpp.o: /home/omar/arduino/hardware/teensy/avr/cores/teensy3/IntervalTimer.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-g++" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD -fno-exceptions -fpermissive -felide-constructors -std=gnu++14 -Wno-error=narrowing -fno-rtti -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL "-I/home/omar/workspace/arduino/test_mux/Release/pch"   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 -x c++ "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

core/core/Print.cpp.o: /home/omar/arduino/hardware/teensy/avr/cores/teensy3/Print.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-g++" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD -fno-exceptions -fpermissive -felide-constructors -std=gnu++14 -Wno-error=narrowing -fno-rtti -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL "-I/home/omar/workspace/arduino/test_mux/Release/pch"   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 -x c++ "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

core/core/Stream.cpp.o: /home/omar/arduino/hardware/teensy/avr/cores/teensy3/Stream.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-g++" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD -fno-exceptions -fpermissive -felide-constructors -std=gnu++14 -Wno-error=narrowing -fno-rtti -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL "-I/home/omar/workspace/arduino/test_mux/Release/pch"   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 -x c++ "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

core/core/Tone.cpp.o: /home/omar/arduino/hardware/teensy/avr/cores/teensy3/Tone.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-g++" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD -fno-exceptions -fpermissive -felide-constructors -std=gnu++14 -Wno-error=narrowing -fno-rtti -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL "-I/home/omar/workspace/arduino/test_mux/Release/pch"   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 -x c++ "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

core/core/WMath.cpp.o: /home/omar/arduino/hardware/teensy/avr/cores/teensy3/WMath.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-g++" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD -fno-exceptions -fpermissive -felide-constructors -std=gnu++14 -Wno-error=narrowing -fno-rtti -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL "-I/home/omar/workspace/arduino/test_mux/Release/pch"   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 -x c++ "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

core/core/WString.cpp.o: /home/omar/arduino/hardware/teensy/avr/cores/teensy3/WString.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-g++" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD -fno-exceptions -fpermissive -felide-constructors -std=gnu++14 -Wno-error=narrowing -fno-rtti -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL "-I/home/omar/workspace/arduino/test_mux/Release/pch"   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 -x c++ "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

core/core/analog.c.o: /home/omar/arduino/hardware/teensy/avr/cores/teensy3/analog.c
	@echo 'Building file: $<'
	@echo 'Starting C compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-gcc" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD  -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

core/core/avr_emulation.cpp.o: /home/omar/arduino/hardware/teensy/avr/cores/teensy3/avr_emulation.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-g++" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD -fno-exceptions -fpermissive -felide-constructors -std=gnu++14 -Wno-error=narrowing -fno-rtti -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL "-I/home/omar/workspace/arduino/test_mux/Release/pch"   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 -x c++ "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

core/core/eeprom.c.o: /home/omar/arduino/hardware/teensy/avr/cores/teensy3/eeprom.c
	@echo 'Building file: $<'
	@echo 'Starting C compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-gcc" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD  -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

core/core/keylayouts.c.o: /home/omar/arduino/hardware/teensy/avr/cores/teensy3/keylayouts.c
	@echo 'Building file: $<'
	@echo 'Starting C compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-gcc" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD  -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

core/core/main.cpp.o: /home/omar/arduino/hardware/teensy/avr/cores/teensy3/main.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-g++" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD -fno-exceptions -fpermissive -felide-constructors -std=gnu++14 -Wno-error=narrowing -fno-rtti -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL "-I/home/omar/workspace/arduino/test_mux/Release/pch"   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 -x c++ "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

core/core/math_helper.c.o: /home/omar/arduino/hardware/teensy/avr/cores/teensy3/math_helper.c
	@echo 'Building file: $<'
	@echo 'Starting C compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-gcc" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD  -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

core/core/memcpy-armv7m.S.o: /home/omar/arduino/hardware/teensy/avr/cores/teensy3/memcpy-armv7m.S
	@echo 'Building file: $<'
	@echo 'Starting S compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-gcc" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD -x assembler-with-cpp -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)"  "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

core/core/memset.S.o: /home/omar/arduino/hardware/teensy/avr/cores/teensy3/memset.S
	@echo 'Building file: $<'
	@echo 'Starting S compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-gcc" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD -x assembler-with-cpp -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)"  "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

core/core/mk20dx128.c.o: /home/omar/arduino/hardware/teensy/avr/cores/teensy3/mk20dx128.c
	@echo 'Building file: $<'
	@echo 'Starting C compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-gcc" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD  -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

core/core/new.cpp.o: /home/omar/arduino/hardware/teensy/avr/cores/teensy3/new.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-g++" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD -fno-exceptions -fpermissive -felide-constructors -std=gnu++14 -Wno-error=narrowing -fno-rtti -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL "-I/home/omar/workspace/arduino/test_mux/Release/pch"   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 -x c++ "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

core/core/nonstd.c.o: /home/omar/arduino/hardware/teensy/avr/cores/teensy3/nonstd.c
	@echo 'Building file: $<'
	@echo 'Starting C compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-gcc" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD  -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

core/core/pins_teensy.c.o: /home/omar/arduino/hardware/teensy/avr/cores/teensy3/pins_teensy.c
	@echo 'Building file: $<'
	@echo 'Starting C compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-gcc" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD  -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

core/core/ser_print.c.o: /home/omar/arduino/hardware/teensy/avr/cores/teensy3/ser_print.c
	@echo 'Building file: $<'
	@echo 'Starting C compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-gcc" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD  -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

core/core/serial1.c.o: /home/omar/arduino/hardware/teensy/avr/cores/teensy3/serial1.c
	@echo 'Building file: $<'
	@echo 'Starting C compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-gcc" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD  -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

core/core/serial2.c.o: /home/omar/arduino/hardware/teensy/avr/cores/teensy3/serial2.c
	@echo 'Building file: $<'
	@echo 'Starting C compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-gcc" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD  -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

core/core/serial3.c.o: /home/omar/arduino/hardware/teensy/avr/cores/teensy3/serial3.c
	@echo 'Building file: $<'
	@echo 'Starting C compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-gcc" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD  -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

core/core/serial4.c.o: /home/omar/arduino/hardware/teensy/avr/cores/teensy3/serial4.c
	@echo 'Building file: $<'
	@echo 'Starting C compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-gcc" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD  -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

core/core/serial5.c.o: /home/omar/arduino/hardware/teensy/avr/cores/teensy3/serial5.c
	@echo 'Building file: $<'
	@echo 'Starting C compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-gcc" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD  -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

core/core/serial6.c.o: /home/omar/arduino/hardware/teensy/avr/cores/teensy3/serial6.c
	@echo 'Building file: $<'
	@echo 'Starting C compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-gcc" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD  -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

core/core/serial6_lpuart.c.o: /home/omar/arduino/hardware/teensy/avr/cores/teensy3/serial6_lpuart.c
	@echo 'Building file: $<'
	@echo 'Starting C compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-gcc" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD  -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

core/core/touch.c.o: /home/omar/arduino/hardware/teensy/avr/cores/teensy3/touch.c
	@echo 'Building file: $<'
	@echo 'Starting C compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-gcc" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD  -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

core/core/usb_audio.cpp.o: /home/omar/arduino/hardware/teensy/avr/cores/teensy3/usb_audio.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-g++" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD -fno-exceptions -fpermissive -felide-constructors -std=gnu++14 -Wno-error=narrowing -fno-rtti -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL "-I/home/omar/workspace/arduino/test_mux/Release/pch"   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 -x c++ "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

core/core/usb_desc.c.o: /home/omar/arduino/hardware/teensy/avr/cores/teensy3/usb_desc.c
	@echo 'Building file: $<'
	@echo 'Starting C compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-gcc" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD  -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

core/core/usb_dev.c.o: /home/omar/arduino/hardware/teensy/avr/cores/teensy3/usb_dev.c
	@echo 'Building file: $<'
	@echo 'Starting C compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-gcc" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD  -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

core/core/usb_flightsim.cpp.o: /home/omar/arduino/hardware/teensy/avr/cores/teensy3/usb_flightsim.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-g++" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD -fno-exceptions -fpermissive -felide-constructors -std=gnu++14 -Wno-error=narrowing -fno-rtti -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL "-I/home/omar/workspace/arduino/test_mux/Release/pch"   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 -x c++ "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

core/core/usb_inst.cpp.o: /home/omar/arduino/hardware/teensy/avr/cores/teensy3/usb_inst.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-g++" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD -fno-exceptions -fpermissive -felide-constructors -std=gnu++14 -Wno-error=narrowing -fno-rtti -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL "-I/home/omar/workspace/arduino/test_mux/Release/pch"   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 -x c++ "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

core/core/usb_joystick.c.o: /home/omar/arduino/hardware/teensy/avr/cores/teensy3/usb_joystick.c
	@echo 'Building file: $<'
	@echo 'Starting C compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-gcc" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD  -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

core/core/usb_keyboard.c.o: /home/omar/arduino/hardware/teensy/avr/cores/teensy3/usb_keyboard.c
	@echo 'Building file: $<'
	@echo 'Starting C compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-gcc" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD  -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

core/core/usb_mem.c.o: /home/omar/arduino/hardware/teensy/avr/cores/teensy3/usb_mem.c
	@echo 'Building file: $<'
	@echo 'Starting C compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-gcc" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD  -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

core/core/usb_midi.c.o: /home/omar/arduino/hardware/teensy/avr/cores/teensy3/usb_midi.c
	@echo 'Building file: $<'
	@echo 'Starting C compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-gcc" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD  -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

core/core/usb_mouse.c.o: /home/omar/arduino/hardware/teensy/avr/cores/teensy3/usb_mouse.c
	@echo 'Building file: $<'
	@echo 'Starting C compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-gcc" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD  -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

core/core/usb_mtp.c.o: /home/omar/arduino/hardware/teensy/avr/cores/teensy3/usb_mtp.c
	@echo 'Building file: $<'
	@echo 'Starting C compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-gcc" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD  -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

core/core/usb_rawhid.c.o: /home/omar/arduino/hardware/teensy/avr/cores/teensy3/usb_rawhid.c
	@echo 'Building file: $<'
	@echo 'Starting C compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-gcc" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD  -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

core/core/usb_seremu.c.o: /home/omar/arduino/hardware/teensy/avr/cores/teensy3/usb_seremu.c
	@echo 'Building file: $<'
	@echo 'Starting C compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-gcc" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD  -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

core/core/usb_serial.c.o: /home/omar/arduino/hardware/teensy/avr/cores/teensy3/usb_serial.c
	@echo 'Building file: $<'
	@echo 'Starting C compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-gcc" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD  -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

core/core/usb_touch.c.o: /home/omar/arduino/hardware/teensy/avr/cores/teensy3/usb_touch.c
	@echo 'Building file: $<'
	@echo 'Starting C compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-gcc" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD  -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

core/core/yield.cpp.o: /home/omar/arduino/hardware/teensy/avr/cores/teensy3/yield.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"/home/omar/arduino/hardware/teensy/../tools/arm/bin/arm-none-eabi-g++" -c -O3 -g -Wall -ffunction-sections -fdata-sections -nostdlib -MMD -fno-exceptions -fpermissive -felide-constructors -std=gnu++14 -Wno-error=narrowing -fno-rtti -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -D__MK66FX1M0__ -DTEENSYDUINO=146 -DARDUINO=10802 -DF_CPU=180000000 -DUSB_MIDI_AUDIO_SERIAL -DLAYOUT_US_INTERNATIONAL "-I/home/omar/workspace/arduino/test_mux/Release/pch"   -I"/home/omar/arduino/hardware/teensy/avr/cores/teensy3" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Wire" -I"/home/omar/Arduino/libraries/MUX74HC4067" -I"/home/omar/Arduino/libraries/movingAvg/src" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/Audio" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD/utility" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SD" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SerialFlash" -I"/home/omar/arduino/hardware/teensy/avr/libraries/SPI" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 -x c++ "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '


