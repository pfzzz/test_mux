#include "Arduino.h"
#include "MicroDexedControl.h"
#include "Analog.h"
#include "dexed.h"
#include "config.h"

Dexed *dexed = new Dexed(SAMPLE_RATE);
MicroDexedControl control(dexed);
Analog analog;

void setup() {
	Serial.begin(SERIAL_SPEED);
	while (!Serial) ;

	analog.init(&control);
}

void loop() {
	analog.process();
}
