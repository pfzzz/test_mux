

#ifndef AnalogPoti_h
#define AnalogPoti_h

#if defined(ARDUINO) && ARDUINO >= 100
#include "Arduino.h"
#else
#include "WProgram.h"
#endif

#define PRINT_DEBUG_ANALOG_POTI                         0
#define PRINT_DEBUG_ANALOG_POTI_PARAMTER_VALUE_CHANGE   1

#define ACTION_START_THRESHOLD    20
#define ACTION_LONGER_THRESHOLD   10
#define ACTION_DURATION_MS        1000


class AnalogPoti
{
public:

    AnalogPoti( uint8_t in_pin, uint16_t in_resolution, uint16_t in_map_to_max );

    bool read( int* value );
    //  returns true, if 'value' was set to the new (mapped) parameter value

private:

    uint8_t pin;
    uint16_t resolution;
    uint16_t map_to_max;

    int poti_value;
    unsigned long poti_actionStartTime;

    int map( int inputValue, int maxInput, int maxOutput );

    int parameter_value;        //  the mapped poti value (mapped to map_to_max)
    //  this value is set in read() for 'value'
    int last_parameter_value;
};

#endif
