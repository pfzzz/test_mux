/*
 * Analog.h
 *
 *  Created on: Dec 13, 2019
 *      Author: omar
 */

#ifndef ANALOG_H_
#define ANALOG_H_

#include "MicroDexedControl.h"
#include <movingAvg.h>
#include <Arduino.h>
#include <ResponsiveAnalogRead.h>
#include <MUX74HC4067.h>

# define MAX_POTS 8

class Analog {
private:
    MicroDexedControl *control;
    ResponsiveAnalogRead *pots_avg[MAX_POTS];
    int manual_switch_pin = 24;
    MUX74HC4067* mux;

    void update_parameter(int pot_id, int pot_value);

public:
    void init(MicroDexedControl *ext_control) {
    	this->mux->signalPin(38, INPUT, ANALOG);
        control = ext_control;
        for (int i=0; i < MAX_POTS; i++) {
        	this->pots_avg[i] = new ResponsiveAnalogRead();
        	this->pots_avg[i]->begin(this->mux, i, true, 0.2);
        }
        pinMode(this->manual_switch_pin, INPUT);
    };

    Analog() {
    	this->mux = new MUX74HC4067(25, 2, 3, 4, 5);
    }

    void process();

    int readPot(const byte pin);
    bool Analog::hasChanged(const byte idx);

};

bool Analog::hasChanged(const byte idx)
{
	static int previousVals[MAX_POTS] = {0};
	int previousReading = previousVals[idx];
	int value;
	int data = (int) pots_avg[idx]->getRawValue();
	if (data < (previousReading * 0.9) || data > (previousReading * 1.1)) {
		int data = (int) pots_avg[idx]->getRawValue();
		previousVals[idx] = data;
		return true;
	}
	return false;
}

int Analog::readPot(const byte idx) {
    static int previousVals[10] = {0};
    int previousReading = previousVals[idx];
    int data = (int) pots_avg[idx]->getRawValue();
    if (data+1 != previousReading && data-1 != previousReading) {
        int data = (int) pots_avg[idx]->getRawValue();
        previousVals[idx] = data;
        previousReading = data;
    }
    return previousReading;
}

void Analog::process() {
    static int counter = 0;
    for (byte i = 0; i < MAX_POTS; ++i)
    {
    	delay(400);
    	// Reads from channel i. Returns a value from 0 to 1023
    	int data = this->mux->read(i);

    	Serial.print("Potentiometer at channel ");
    	Serial.print(i);
    	Serial.print(" is at ");
    	Serial.print(map(data, 0, 1023, 0, 100));
    	Serial.println("%%");
    }
    Serial.println("\n\n\n");
//    int manually_on = digitalRead(this->manual_switch_pin);
//    if (manually_on) {
//    	Serial.println("on");

//    for (int i = 0; i < MAX_POTS; i++) {
//    	pots_avg[i]->update();
//    	bool changed = this->hasChanged(i);
//    	if (changed) {
//    		Serial.println("pot");
//    		Serial.println(i);
//    		Serial.println(pots_avg[i]->mux->read(i));
//    		Serial.println(pots_avg[i]->getRawValue());
//    	}

    	//            if (counter % 150 == 0) {
    	//                bool changed = pots_avg[i].hasChanged();
    	//                if (changed) {
    	//                    int pot_val = map(
    	//                            pots_avg[i].getValue(), 0, 1023, 0,
    	//                            127);
    	//                    Serial.println(pot_val);
    	//                    //this->update_parameter(i, pot_val);
    	//                }
    	//            }
//    }
    //    } else {
    //    	Serial.println("off");
    //    }
//    delay(1000);
    counter++;
}

void Analog::update_parameter(int pot_id, int pot_value) {
    switch (pot_id) {
        case 0:
            control->update_coarse_tuning(1, pot_value);
            break;
        case 1:
            control->update_coarse_tuning(2, pot_value);
            break;
        case 2:
            control->update_coarse_tuning(3, pot_value);
            break;
        case 3:
            control->update_output_level(2, pot_value);
            break;
        case 4:
            control->update_output_level(3, pot_value);
            break;
    }
}

#endif /* ANALOG_H_ */
